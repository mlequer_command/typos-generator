<?php

namespace MLequer;

use MLequer\AppKernel;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApplicationTest extends TestCase
{

    /**
     * @covers \MLequer\Command\TyposGeneratorCommand
     * @covers \MLequer\AppKernel
     * @covers \MLequer\DependencyInjection\CompilerPass\CollectCommandsToApplicationCompilerPass
     *
     */
    public function testAutoloadingOfCommmand(): void
    {
        $kernel = new AppKernel('dev', true);
        $kernel->boot();
        /** @var ContainerInterface $container */
        $container = $kernel->getContainer();

        /** @var Application $application */
        $application = $container->get(Application::class);
        $this->assertNotFalse($application);
        $command = $application->find('typos:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                "word" => "computer",
                "--wrong-keys" => true
            ]
        );

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('compute4', $output);
    }
}
