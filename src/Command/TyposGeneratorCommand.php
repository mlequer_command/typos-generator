<?php

/**
 * console-skeleton
 * @author  michel <michel@mlequer.com> - https://mlequer.com
 * @license see LICENSE included in package
 * @version 1.0.0
 *
 **/

namespace MLequer\Command;

use MLequer\Component\Typos\Generator\TypoGenerator;
use MLequer\Component\Typos\Provider\BitFlippingTyposGenerator;
use MLequer\Component\Typos\Provider\ChainTyposProvider;
use MLequer\Component\Typos\Provider\DoubleCharTyposProvider;
use MLequer\Component\Typos\Provider\HomophoneTyposProvider;
use MLequer\Component\Typos\Provider\MissedCharTyposProvider;
use MLequer\Component\Typos\Provider\TransposedCharTyposProvider;
use MLequer\Component\Typos\Provider\TyposProviderCollection;
use MLequer\Component\Typos\Provider\WrongKeyTyposProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TyposGeneratorCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'typos:generate';

    /**
     * Mapping between options providers
     * @var array<string, string>
     */
    private $optionsMap = [
        'wrong-keys' => WrongKeyTyposProvider::class,
        'missed-chars' => MissedCharTyposProvider::class,
        'transposed-chars' => TransposedCharTyposProvider::class,
        'double-chars' => DoubleCharTyposProvider::class,
        'flip-bits' => BitFlippingTyposGenerator::class,
        'generate-homophones' => HomophoneTyposProvider::class,
    ];

    protected function configure(): void
    {
        $this
            ->setDescription('Return generated typos for a word')
            ->addArgument('word', InputArgument::REQUIRED, 'Word')
            ->addOption('wrong-keys', 'w', InputOption::VALUE_NONE, 'generate wrong key typos ')
            ->addOption('missed-chars', 'm', InputOption::VALUE_NONE, 'generate missed chars typos')
            ->addOption('transposed-chars', 't', InputOption::VALUE_NONE, 'generate transposed chars typos')
            ->addOption('double-chars', 'd', InputOption::VALUE_NONE, 'generate double chars typos')
            ->addOption('flip-bits', 'f', InputOption::VALUE_NONE, 'generate flip-bits typos')
            ->addOption('generate-homophones', 'g', InputOption::VALUE_NONE, 'generate homophones typos');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $word */
        $word = $input->getArgument('word');
        $chainProvider = new ChainTyposProvider($this->getProviders($input));
        $generator = new TypoGenerator($chainProvider);

        $typos = $generator->generateTyposAsArray($word);
        $output->writeln($typos);

        return 0;
    }

    /**
     * Prepare the collection for the chain provider
     * @param InputInterface $input
     * @return TyposProviderCollection the collection of providers to use
     */
    private function getProviders(InputInterface $input): TyposProviderCollection
    {
        $genCollection = new TyposProviderCollection();

        foreach ($this->optionsMap as $option => $className) {
            if (!$input->getOption($option)) {
                continue;
            }
            $genCollection->addProvider(new $className());
        }

        return $genCollection;
    }
}
