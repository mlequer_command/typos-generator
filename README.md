[![pipeline status](https://gitlab.com/mlequer_command/typos-generator/badges/master/pipeline.svg)](https://gitlab.com/mlequer_command/typos-generator/-/commits/master)
[![coverage report](https://gitlab.com/mlequer_command/typos-generator/badges/master/coverage.svg)](https://gitlab.com/mlequer_command/typos-generator/-/commits/master)

# Typos generator command line tool

## Installation

Install the latest version with

`$ composer require mlequer/typos-generator-command`


## Usage

```shell script
$ bin/console typos:generate something -gd

ssomething
soomething
sommething
someething
sometthing
somethhing
somethiing
somethinng
somethingg
somethighng
somethinng
sewmething
sowmething
sumthing

```

view the command line help `--help` for options.

```shell script
$ bin/console typos:generate --help     

Description:
  Return generated typos for a word

Usage:
  typos:generate [options] [--] <word>

Arguments:
  word                       Word

Options:
  -w, --wrong-keys           generate wrong key typos 
  -m, --missed-chars         generate missed chars typos
  -t, --transposed-chars     generate transposed chars typos
  -d, --double-chars         generate double chars typos
  -f, --flip-bits            generate flip-bits typos
  -g, --generate-homophones  generate homophones typos
  -h, --help                 Display this help message
  -q, --quiet                Do not output any message
  -V, --version              Display this application version
      --ansi                 Force ANSI output
      --no-ansi              Disable ANSI output
  -n, --no-interaction       Do not ask any interactive question
  -v|vv|vvv, --verbose       Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

```



